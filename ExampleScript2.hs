module Scripts.ExampleScript.ExampleScript2 where

import Script


execute1 :: IVIScriptArgs -> IO IVIScriptResult
execute1 _ = 
    do 
        putStrLn "this is testscript 1"
        return Success

execute2 :: IVIScriptArgs -> IO IVIScriptResult
execute2 _ = 
    do 
        putStrLn "this is testscript 2"
        return Success
